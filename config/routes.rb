Rails.application.routes.draw do
  devise_for :users
  namespace :api do
    namespace :v1 do
      resources :rules
      resources :users, :only => [:show, :create, :update, :destroy]
      resources :sessions, :only => [:create, :destroy]
    end
  end

  root to: 'dashboard#show'
end
