require 'rails_helper'

RSpec.describe Api::V1::RulesController, :type => :controller do
  describe "POST#create" do

    context "authentication token provided" do
      before :each do
        @user = create :user
        request.headers['Authorization'] = @user.auth_token
        @rule_attributes = attributes_for :rule
        
      end

      context "with a valid authentication token" do
        it "creates a rule if rule doesnt exist" do
          expect{
            post :create , @rule_attributes ,format: :json
            }.to change(Rule,:count).by(1)
        end

        it "does not create a rule if rule exists" do
          expect{
          post :create , @rule_attributes ,format: :json
          }.to change(Rule,:count).by(1)

          expect{
          post :create , @rule_attributes ,format: :json
          }.to change(Rule,:count).by(0)
        end
      end

      context "with an invalid authentication token" do
        
        it "does not create a rule" do
          request.headers['Authorization'] = "jfgiebfgiweunrfldmcoawfin"
          expect{
          post :create , @rule_attributes ,format: :json
          }.to change(Rule,:count).by(0)
        end
      end
    end

    context "authentication token not provided" do
      it "does not create a rule" do
        post :create , @rule_attributes ,format: :json
        expect(response).to have_http_status(401)
      end
    end
  end
end
