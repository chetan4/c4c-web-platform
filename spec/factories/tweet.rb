FactoryGirl.define do
  factory :tweet do
    twitter_tweet_id "2005:347769243409977344"
    actor_id 1
    body "tweet text"
    link "http:\/\/twitter.com\/KidCodo\/statuses\/347769243409977344"
    verb "post"
    posted_time "2013-06-25T17:12:52.000Z"
  end
end