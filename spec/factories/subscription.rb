FactoryGirl.define do
  factory :subscription do
    company_id 1
    plan_id 1
    start_date DateTime.now
    end_date   DateTime.now + 1
  end
end