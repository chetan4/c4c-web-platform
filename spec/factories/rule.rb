FactoryGirl.define do
  factory :rule do
    rule { Faker::Lorem.word }
  end
end
