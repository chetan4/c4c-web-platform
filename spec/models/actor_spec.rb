require 'rails_helper'

describe Actor do
  describe "link Validation" do
    it "should be invalid if link blank" do
      link = build(:actor,link: nil)
      expect(link).to be_invalid
    end

    it "should be valid if link present" do
      link = build(:actor )
      expect(link).to be_valid
    end
  end

  describe "twitter_user_id validation" do
    it "should be invalid if id blank" do
      user_id = build(:actor , twitter_user_id: nil)
      expect(user_id).to be_invalid
    end
    it "should be valid if id present" do
      user_id = build(:actor)
      expect(user_id).to be_valid
    end
  end

  describe "image_url validation" do
    it "should be invalid if image_url blank" do
      image_url = build(:actor , image_url: nil)
      expect(image_url).to be_invalid
    end
    it "should be valid if image_url present" do
      image_url = build(:actor)
      expect(image_url).to be_valid
    end
  end

  describe "summary validation" do
    it "should be invalid if summary is blank" do
      summary = build(:actor ,summary: nil)
      expect(summary).to be_invalid
    end
    it "should be valid if summary present" do
      summary = build(:actor)
      expect(summary).to be_valid
    end
  end
end
