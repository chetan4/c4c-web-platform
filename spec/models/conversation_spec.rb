require 'rails_helper'

describe Conversation do
  it "should be invalid if parent_tweet_id is blank" do
    conversation = build(:conversation, parent_tweet_id: nil)
    expect(conversation).to be_invalid
  end

  it "should be valid if parent_tweet_id is present" do
    conversation = build(:conversation)
    expect(conversation).to be_valid
  end

end
