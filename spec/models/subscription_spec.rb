require 'rails_helper'

describe Subscription do 
  describe "company_id validation" do 
    it "should be invalid if company_id blank" do 
      company_id = build(:subscription,company_id: nil)
      expect(company_id).to be_invalid
    end
    it "should be valid if company_id present" do
      company_id = build(:subscription)
      expect(company_id).to be_valid
    end
  end

  describe "plan_id validation" do
    it "should be invalid if plan_id blank" do
      plan_id = build(:subscription , plan_id:nil)
      expect(plan_id).to be_invalid
    end

    it "should be valid if plan_id present" do
      plan_id = build(:subscription )
      expect(plan_id).to be_valid
    end
  end

  describe "start_date validation" do
    it "should be invalid if start_date blank" do
      start_date = build(:subscription ,start_date: nil)
      expect(start_date).to be_invalid
    end

    it "should be valid if start_date present" do
      start_date = build(:subscription)
      expect(start_date).to be_valid
    end
  end

  describe "end_date validation" do
    it "should be invalid if end_date blank" do 
      end_date = build(:subscription ,end_date:nil)
      expect(end_date).to be_invalid
    end
    
    it "should be valid if end_date present" do
      end_date = build(:subscription)
      expect(end_date).to be_valid
    end
  end
end