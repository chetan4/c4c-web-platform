require 'rails_helper'

RSpec.describe User, :type => :model do
  before { @user = build(:user) }

  subject { @user }

  it { should respond_to(:email) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }

  it { should be_valid } 

  it { should validate_uniqueness_of(:auth_token) } 

  describe "#generate_authentication_token!" do
    it "generates a unique token" do
      Devise.stub(:friendly_token).and_return("uniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).to eql "uniquetoken123"
    end

    it "generates a new token when one already has been taken" do
      existing_user = create(:user, auth_token:"uniquetoken123")
      @user.generate_authentication_token!
      expect(@user.auth_token).not_to eql existing_user.auth_token
    end
  end

end
