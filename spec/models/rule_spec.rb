require 'rails_helper'

 
describe Rule do
	
	it "should be invalid if rule is blank" do
		rule = build(:rule,rule: nil)
		expect(rule).to be_invalid
	end

	it "should be valid if rule is present" do
		rule = build(:rule)
		expect(rule).to be_valid
	end

	it "should be unique" do
	  create(:rule,rule: '#rails' )
	  rule = build(:rule, rule: '#rails')
	  rule.valid?
	  expect(rule.errors[:rule]).to include("has already been taken")
	end
end

