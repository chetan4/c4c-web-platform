require 'rails_helper'

describe Tweet do

  describe "twitter_tweet_id validation" do
    it "should be invalid if twitter_tweet_id is blank" do
      tweet_id = build(:tweet,twitter_tweet_id: nil)
      expect(tweet_id).to be_invalid
    end

    it "should be valid if twitter_tweet_id is present" do
      tweet_id = build(:tweet)
      expect(tweet_id).to be_valid
    end
  end

  describe "actor_id validation" do
    it "should be invalid if actor_id is blank" do
      actor_id = build(:tweet , actor_id: nil)
      expect(actor_id).to be_invalid
    end

    it "should be valid if actor_id present" do
      actor_id = build(:tweet)
      expect(actor_id).to be_valid
    end
  end
  
  describe "body validation" do
    it "should be invalid if body text blank" do
      body = build(:tweet , body: nil)
      expect(body).to be_invalid
    end

    it "should be valid if body text present" do
      body =build(:tweet)
      expect(body).to be_valid
    end
  end

  describe "verb validation" do
    it "should be invalid if verb is blank" do 
      verb = build(:tweet ,verb: nil)
      expect(verb).to be_invalid
    end 

    it "should be valid if verb present" do
      verb = build(:tweet)
      expect(verb).to be_valid
    end
  end

  describe "link validation" do 
    it "should be invalid if link blank" do 
      link = build(:tweet , link: nil)
      expect(link).to be_invalid
    end

    it "should be valid if link is present" do
      link = build(:tweet)
      expect(link).to be_valid
    end
  end

  describe "posttime validation" do 
    it "should be invalid if posttime is blank" do 
      post_time = build(:tweet , posted_time:nil)
      expect(post_time).to be_invalid
    end

    it "should be valid if posttime present" do
      post_time = build(:tweet)
      expect(post_time).to be_valid
    end
  end
end
