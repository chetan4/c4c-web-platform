require 'rails_helper'

describe TweetTweetType do 

  describe "tweet_id validation" do 
    it "should be invalid if tweet_id blank" do
      tweet_id = build(:tweet_tweet_type,tweet_id: nil)
      expect(tweet_id).to be_invalid
    end

    it "should be valid if tweet_id present" do
      tweet_id = build(:tweet_tweet_type)
      expect(tweet_id).to be_valid
    end
  end
  
  describe "tweet_type_id validation" do
    it "should be invalid if tweet_type_id blank" do
      tweet_type_id = build(:tweet_tweet_type,tweet_type_id: nil)
      expect(tweet_type_id).to be_invalid
    end

    it "should be valid if tweet_type_id present" do
      tweet_type_id = build(:tweet_tweet_type)
      expect(tweet_type_id).to be_valid
    end
  end
end
