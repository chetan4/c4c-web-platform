class AddParentTweetIdToConversations < ActiveRecord::Migration
  def change
    add_column :conversations, :parent_tweet_id, :string
  end
end
