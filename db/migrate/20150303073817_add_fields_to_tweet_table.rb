class AddFieldsToTweetTable < ActiveRecord::Migration
  def change
    add_column :tweets, :twitter_tweet_id, :string, null: false
    add_column :tweets, :actor_id, :integer, null: false
    add_column :tweets, :body, :string, null: false
    add_column :tweets, :in_reply_to, :string
    add_column :tweets, :link, :string, null: false
    add_column :tweets, :verb, :string, null: false
    add_column :tweets, :posted_time, :datetime, null:false

    add_index :tweets, :twitter_tweet_id
    add_index :tweets, :actor_id
  end
end
