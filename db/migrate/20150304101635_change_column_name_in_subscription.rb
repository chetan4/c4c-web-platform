class ChangeColumnNameInSubscription < ActiveRecord::Migration
  def up
  	rename_column :subscriptions, :comapany_id, :company_id
  end
end
