class CreateAuthenticationTokens < ActiveRecord::Migration
  def change
    create_table :authentication_tokens do |t|
      t.string :access_token

      t.timestamps null: false
    end
  end
end
