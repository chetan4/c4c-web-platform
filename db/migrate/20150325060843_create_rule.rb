class CreateRule < ActiveRecord::Migration
  def change
    create_table :rules do |t|
      t.string :rule
      t.timestamps null:false
    end
  end
end
