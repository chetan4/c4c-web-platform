class CreateTweetTweetTypes < ActiveRecord::Migration
  def change
    create_table :tweet_tweet_types do |t|
      t.integer :tweet_id
      t.integer :tweet_type_id

      t.timestamps null: false
    end
  end
end
