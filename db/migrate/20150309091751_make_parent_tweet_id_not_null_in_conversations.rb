class MakeParentTweetIdNotNullInConversations < ActiveRecord::Migration
  def change
    change_column :conversations, :parent_tweet_id, :integer, null: false
  end
end
