class CreateActors < ActiveRecord::Migration
  def change
    create_table :actors do |t|
      t.string :link, null: false
      t.string :twitter_user_id, null: false
      t.string :image_url, null: false
      t.string :summary
      t.timestamps null: false
    end

    add_index :actors, [:twitter_user_id]
  end
end
