class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :plan_id
      t.integer :comapany_id
      t.boolean :active
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps null: false
    end
  end
end
