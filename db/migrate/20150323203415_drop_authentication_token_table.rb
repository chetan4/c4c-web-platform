class DropAuthenticationTokenTable < ActiveRecord::Migration
  def change
    drop_table :authentication_tokens
  end
end
