class AddConversationIdToTweets < ActiveRecord::Migration
  def change
    add_column :tweets, :conversation_id, :integer
  end
end
