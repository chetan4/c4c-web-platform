# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150325083529) do

  create_table "actors", force: :cascade do |t|
    t.string   "link",            limit: 255, null: false
    t.string   "twitter_user_id", limit: 255, null: false
    t.string   "image_url",       limit: 255, null: false
    t.string   "summary",         limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "actors", ["twitter_user_id"], name: "index_actors_on_twitter_user_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.integer  "subscription_id", limit: 4
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "conversations", force: :cascade do |t|
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "parent_tweet_id", limit: 4, null: false
  end

  create_table "plans", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "no_of_keywords", limit: 4
    t.integer  "price",          limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "reply_tweets", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "tweet_id",       limit: 4, null: false
    t.integer  "reply_tweet_id", limit: 4, null: false
  end

  create_table "rules", force: :cascade do |t|
    t.string   "rule",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "plan_id",    limit: 4
    t.boolean  "active",     limit: 1
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "company_id", limit: 4
  end

  create_table "tweet_tweet_types", force: :cascade do |t|
    t.integer  "tweet_id",      limit: 4
    t.integer  "tweet_type_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tweet_types", force: :cascade do |t|
    t.string   "type",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "tweets", force: :cascade do |t|
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "twitter_tweet_id", limit: 255, null: false
    t.integer  "actor_id",         limit: 4,   null: false
    t.string   "body",             limit: 255, null: false
    t.string   "in_reply_to",      limit: 255
    t.string   "link",             limit: 255, null: false
    t.string   "verb",             limit: 255, null: false
    t.datetime "posted_time",                  null: false
    t.integer  "conversation_id",  limit: 4
  end

  add_index "tweets", ["actor_id"], name: "index_tweets_on_actor_id", using: :btree
  add_index "tweets", ["twitter_tweet_id"], name: "index_tweets_on_twitter_tweet_id", using: :btree

  create_table "user_rules", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "rule_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "auth_token",             limit: 255, default: ""
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
