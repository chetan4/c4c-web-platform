class Subscription < ActiveRecord::Base
  validates_presence_of :company_id ,:plan_id ,:start_date ,:end_date
  belongs_to :company
  belongs_to :plan
end
