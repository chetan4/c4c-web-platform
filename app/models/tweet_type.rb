class TweetType < ActiveRecord::Base
	has_many :tweet_tweet_types 
	has_many :tweets , :through => :tweet_tweet_types 
end
