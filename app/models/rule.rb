class Rule < ActiveRecord::Base
	validates_presence_of :rule
	validates_uniqueness_of :rule
	has_many :user_rules 
  has_many :users ,:through => :user_rules
end
