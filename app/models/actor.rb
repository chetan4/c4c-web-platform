class Actor < ActiveRecord::Base
  has_many :tweets
  validates_presence_of :link ,:twitter_user_id ,:image_url ,:summary
end
