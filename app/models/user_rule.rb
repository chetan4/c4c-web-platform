class UserRule < ActiveRecord::Base
	belongs_to :user
  belongs_to :rule
  validates_presence_of :rule_id , :user_id
end
