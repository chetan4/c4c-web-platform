class TweetTweetType < ActiveRecord::Base
  belongs_to :tweet 
  belongs_to :tweet_type
  validates_presence_of :tweet_id , :tweet_type_id
end
