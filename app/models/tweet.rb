class Tweet < ActiveRecord::Base
  validates_presence_of :twitter_tweet_id ,:actor_id,:body,:link,:verb,:posted_time
  belongs_to :actor
  belongs_to :conversation
  has_many :reply_tweets
  has_many :tweet_tweet_types 
  has_many :tweet_types , :through => :tweet_tweet_types
end
