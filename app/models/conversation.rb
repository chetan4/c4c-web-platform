class Conversation < ActiveRecord::Base
  validates_presence_of :parent_tweet_id
  has_many :tweets, dependent: :destroy
end
