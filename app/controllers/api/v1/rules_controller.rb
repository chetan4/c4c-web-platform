module Api
  module V1
    class RulesController < ApplicationController
      
     before_action :authenticate_with_token!

     #Used to add a new rule
     def create
	rule = Rule.new(rule: params[:rule])
	if rule.save
	  #TODO: Send rule to stream fetcher api
	  render json: rule, status: 201
	else
	  render json: rule.errors, status: 422
	end
      end
 
    end
  end
end
